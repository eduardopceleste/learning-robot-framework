*** Settings ***
Documentation  Login
...  continuando o comentário acima
Library  SeleniumLibrary
##### Test Setup  Abrir browser
Test Teardown  Close browser

*** Variables ***
${browser_default}  chrome
${url}

*** Test Cases ***
Cenário: login
    [Tags]  login_valido
    Abrir browser
    Inserir login
    Inserir a senha
    Clicar no botão Login
    Validar se fez login corretamente

Cenário: Validating URL
    [Tags]  validating_URL
    Abrir browser 2
    Clicar no menu Home
    ${url}=  Captura URL
    Valida URL


*** Keywords ***
Abrir browser
    open browser  https://the-internet.herokuapp.com/login  ${browser_default}

Abrir browser 2
    open browser  https://the-internet.herokuapp.com/floating_menu  ${browser_default}

Inserir login
    Input text  id=username  tomsmith

Inserir a senha
    Input text  id=password  SuperSecretPassword!

Clicar no botão Login
    Click Button  Login

Validar se fez login corretamente
    Element should contain  id=flash  You logged into a secure area!

Captura URL
    Get Location

Valida URL
    Location Should Contain  home

Clicar no menu Home
    Click Element  css=#menu li:first-child